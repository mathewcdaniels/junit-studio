import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class RomanNumeralTest {

    @Test(expected = IllegalArgumentException.class)
    public void romanNumeralTestForPositive() {
        RomanNumeral.fromInt(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void romanNumeralTestForNotZero() {
        RomanNumeral.fromInt(0);
    }

    @Test
    public void romanNumeralTest() {
        assertEquals("I", RomanNumeral.fromInt(1));
        assertEquals("IV", RomanNumeral.fromInt(4));
        assertEquals("IX", RomanNumeral.fromInt(9));
        assertEquals("XL", RomanNumeral.fromInt(40));
        assertEquals("LI", RomanNumeral.fromInt(51));
        assertEquals("CMXCIX", RomanNumeral.fromInt(999));
        assertEquals("MDCLXVI", RomanNumeral.fromInt(1666));
        assertEquals("MM", RomanNumeral.fromInt(2000));
    }
}
