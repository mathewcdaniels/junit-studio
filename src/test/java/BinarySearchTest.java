import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BinarySearchTest {

    @Test
    public void binarySearchTest() {
        int[] sortedNumbers = {4, 5, 9, 11, 15, 21, 54, 62};
        for (int i = 0; i < sortedNumbers.length; i++) {
            assertEquals(i, BinarySearch.binarySearch(sortedNumbers, sortedNumbers[i]));
        }
    }
}
