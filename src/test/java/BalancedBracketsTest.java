import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    @Test
    public void balancedBracketsTest() {
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("[This is a string]"));
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("[This [is] a string]"));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("[This is[[[[]] a] string]"));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("[This [is] [a] [string]]["));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("][]"));
    }

}
